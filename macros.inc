macro clear_screen
{
    push ax

    mov ax,3
    int 10h

    pop ax
}

macro print_zs
{
;ah contains color byte
;ds:bx=string address
    local put_char, str_end
    put_char:
        mov al,[bx]
        inc bx
        test al,al
        jz str_end
        mov [es:di],ax
        add di,2
        jmp put_char
    str_end:
}

macro master_irq_handler
{
    push ax

    mov al,20h
    out PIC1_CMD,al

    pop ax
    iret
}

macro slave_irq_handler
{
    push ax

    mov al,20h
    out PIC2_CMD,al
    out PIC1_CMD,al

    pop ax
    iret
}
