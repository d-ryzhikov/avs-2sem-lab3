format MZ
entry main:start
;org 100h

include "selectors.inc"
include "macros.inc"

PIC1_CMD  = 20h
PIC1_DATA = 21h
PIC2_CMD  = 0A0h
PIC2_DATA = 0A1h

segment main use16
set_descriptor:
;eax=segment address
;ds:bx=descriptor address
;cl=access rights byte
;edx=limit

    push cx             ;save cx value for further use
    mov cx,ax           ;cx=2 lower bytes of address
    shl ecx,16          ;move cx to upper bytes of ecx
    mov cx,dx           ;cx=0..15 bits of limit
    mov [bx],ecx        ;save 4 lower bytes of descriptor
    shr eax,16          ;ax=2 higher bytes of address
    mov ch,ah           ;ch=4th byte of address
    shr edx,16          ;dl=0000+16..19 bits of limit
    mov cl,dl
    shl ecx,16          ;move cx to upper bytes of ecx
    mov cl,al           ;cl=3rd byte of address
    pop ax              ;al=access rights byte
    mov ch,al
    mov [bx+4],ecx      ;save 4 higher bytes of descriptor
    add bx,8            ;point at next descriptor

    ret

set_IDT_int:
;eax=offset
;ds:bx=descriptor address
;ch=access rights byte
;dx=segment selector

    push cx             ;save access rights byte for later use
    mov cx,dx           ;cx=segment selector
    shl ecx,16          ;move cx to upper 16 bits of ecx
    mov cx,ax           ;cx=lower 16 bits of offset
    mov [bx],ecx        ;save 4 lower bytes of descriptor
    pop ax              ;ah=access rights byte
    mov [bx+4],eax
    ret

start:
    mov ax,main
    mov ds,ax

;save code segment and back_to_rm label value to restore from protected mode
    mov [rm_segment],cs
    lea ax,[back_to_rm]
    mov [rm_offset],ax

;save register values
    mov [rm_ss],ss
    mov [rm_ds],ds
    mov [rm_es],es
    mov [rm_fs],fs
    mov [rm_gs],gs

    xor eax,eax
    mov ax,cs
    shl eax,4           ;eax=physical address of main segment
    push eax
    lea bx,[GDT+8]      ;skip null descriptor
    mov edx,0FFFFh      ;set limit to 64 Kb - 1
    mov cl,10011000b    ;P=1, DPL=00b, S=1, Type=100b, A=0
    call set_descriptor ;set code segment descriptor

    pop eax
    push eax
    xor edx,edx
    lea dx,[stack_start]
    add eax,edx         ;eax=physical address of stack beginning
    mov edx,1024        ;set limit to 1024 bytes
    mov cl,10010110b    ;P=1, DPL=00b, S=1, Type=011b, A=0
    call set_descriptor ;set stack segment descriptor

    pop eax
    push eax
    xor edx,edx
    xor ecx,ecx
    lea dx,[data_end]
    lea cx,[data_start]
    add eax,ecx
    sub dx,cx           ;compute limit of data segment
    mov cl,10010010b    ;P=1, DPL=00b, S=1, Type=001b, A=0
    call set_descriptor ;set data segment descriptor

    mov eax,0b8000h     ;video memory address
    mov edx,4000        ;video memory segment limit
    mov cl,10010010b    ;P=1, DPL=00b, S=1, Type=001b, A=0
    call set_descriptor ;set video memory segment

    pop eax
    push eax
    mov edx,0FFFFh
    mov cl,10011010b    ;P=1, DPL=00b, S=1, Type=101b, A=0
    call set_descriptor ;set r-mode code descriptor

    pop eax
    push eax
    mov edx,0FFFFh
    mov cl,10010010b    ;P=1, DPL=00b, S=1, Type=001b, A=0
    call set_descriptor ;set r-mode data descriptor

    ;set IRQ handlers' descriptors
    xor ebx,ebx
    lea bx,[IDT+20h]
    mov dx,CODE_SELECTOR
    mov cx,8600h        ;P=1, DPL=00b, S=0, Type=0110b (16-bit)
    xor eax,eax
    mov eax,IRQ_0_handler
    call set_IDT_int
    mov cx,8600h
    mov eax,IRQ_1_handler
    call set_IDT_int

    pop eax
    push eax
    xor edx,edx
    lea dx,[GDT]
    add eax,edx
    mov [GDT_address],eax

    pop eax
    xor edx,edx
    lea dx,[IDT]
    add eax,edx
    mov [IDT_address],eax

    clear_screen

    cli

    lgdt fword [GDTR]
    lidt fword [IDTR]

;store stack pointer just before switching to protected mode
    mov [rm_sp],sp

;enter protected mode
    mov eax,cr0
    or al,1             ;set PE flag to 1
    mov cr0,eax

    db 0eah             ;far jump opcode
    dw start_pm
    dw CODE_SELECTOR

start_pm:
;initialize segment selectors
    mov ax,STACK_SELECTOR
    mov ss,ax
    mov sp,0

    mov ax,DATA_SELECTOR
    mov ds,ax

    mov ax,VIDEOMEM_SELECTOR
    mov es,ax

    mov ah,10010101b
    xor di,di
    xor bx,bx
    print_zs

;redirect IRQs
    mov al,10h          ;init command
    out PIC1_CMD,al
    out PIC2_CMD,al

    mov al,20h          ;PIC1 interrupts start position
    out PIC1_DATA,al
    mov al,28h          ;PIC2 interrupts start position
    out PIC2_DATA,al

    mov al,4            ;PIC1 is master
    out PIC1_DATA,al
    mov al,2            ;PIC2 is slave
    out PIC2_DATA,al

;    mov al,10h          ;8086 mode
;    out PIC1_DATA,al
;    mov al,10h
;    out PIC2_DATA,al

    mov al,11111101b    ;unmask IRQs 0 and 1 (timer and keyboard)
    out PIC1_DATA,al
    out PIC2_DATA,al

    sti

    xor bx,bx
main_cycle:
    cmp [input_completed],2
    jne main_cycle

prepare_for_rm:
    cli

    mov ax,RM_DATA_SELECTOR
    mov ss,ax
    mov ds,ax
    mov es,ax
    mov fs,ax
    mov gs,ax

    mov eax,cr0
    xor al,1            ;set PE flag to 0
    mov cr0,eax

    lidt fword [IDTR_realmode]

    db 0eah             ;far jump opcode
    rm_offset dw ?      ;will be filled before
    rm_segment dw ?     ;entering protected mode

back_to_rm:
;restore registers' state
    mov ss,[rm_ss]
    mov ds,[rm_ds]
    mov es,[rm_es]
    mov fs,[rm_fs]
    mov gs,[rm_gs]

;redirect IRQs
    mov al,10h          ;init command
    out PIC1_CMD,al
    out PIC2_CMD,al

    mov al,8            ;PIC1 interrupts start position
    out PIC1_DATA,al
    mov al,70h          ;PIC2 interrupts start position
    out PIC2_DATA,al

    mov al,4            ;PIC1 is master
    out PIC1_DATA,al
    mov al,2            ;PIC2 is slave
    out PIC2_DATA,al

    mov al,0
    out PIC1_DATA,al
    out PIC2_DATA,al

;    mov al,0FFh
;    out PIC1_DATA,al
;    out PIC2_DATA,al

    sti

    clear_screen

;exit
    mov ax,4c00h
    int 21h

IRQ_0_handler:
    push ax

    mov al,20h          ;send EOI to master PIC
    out PIC1_CMD,al

    pop ax

    cmp [timer_count],18
    jb skip

    mov [timer_count],0

skip:
    inc [timer_count]

    iret

IRQ_1_handler:
    push ax

    in al,60h           ;al=key pressed

    inc [input_completed]
    in al,61h
    mov ah,al
    or al,80h
    out 61h,al
    mov al,ah
    out 61h,al

    mov al,20h          ;send EOI to master PIC
    out PIC1_CMD,al
    pop ax
    iret
;--------------------------------------------------------------------
;registers' state before entering protected mode
rm_ss dw ?
rm_sp dw ?
rm_ds dw ?
rm_es dw ?
rm_fs dw ?
rm_gs dw ?

;GDTR register image
GDTR:
    GDT_limit dw gdt_end - GDT - 1
    GDT_address dd ?
GDT:
    db 8 dup(?);null descriptor
    db 8 dup(?);code segment descriptor
    db 8 dup(?);stack segment descriptor
    db 8 dup(?);data segment descriptor
    db 8 dup(?);video memory segment descriptor
    db 8 dup(?);r-mode code segment descriptor
    db 8 dup(?);r-mode data segment descriptor
gdt_end:

IDTR_realmode:
    dw 03FFh
    dd 0
IDTR:
    IDT_limit dw idt_end - IDT - 1
    IDT_address dd ?
IDT:
    db 47*8 dup(0)
idt_end:

;--------------------------------------------------------------------
;protected mode data segment
data_start:
    db "Ryzhikov",0
    input_completed db 0
    timer_count db 0
data_end:

;--------------------------------------------------------------------
;protected mode stack segment
    db 1024 dup(?);place reserved for stack
stack_start:
